package pt.adro.codegen.controller;

import java.util.List;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.openapitools.api.PetApi;
import org.openapitools.model.Pet;

@Path("/pet")
public class PetApiImpl implements PetApi {

  @Override
  public Response addPet(Pet body) {
    return null;
  }

  @Override
  public Response deletePet(Long petId, String apiKey) {
    return null;
  }

  @Override
  public Response findPetsByStatus(List<String> status) {
    return null;
  }

  @Override
  public Response findPetsByTags(List<String> tags) {
    return null;
  }

  @Override
  public Response getPetById(Long petId) {
    return Response.accepted().entity(new Pet()).build();
  }

  @Override
  public Response updatePet(Pet body) {
    return null;
  }

  @Override
  public Response updatePetWithForm(Long petId, String name, String status) {
    return null;
  }

}
