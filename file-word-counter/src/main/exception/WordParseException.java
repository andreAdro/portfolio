package main.exception;

/** @author Adro */
public class WordParseException extends RuntimeException {
  private static final long serialVersionUID = 8055099242280477458L;

  public WordParseException(String fileName, Throwable cause) {
    super(
        String.format(
            "Unable to parse file [%s]. Something went wrong while parsing the file. Please ensure that the file is in the provided directory",
            fileName),
        cause);
  }
}
