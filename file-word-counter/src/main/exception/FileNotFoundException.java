package main.exception;

/** @author Adro */
public class FileNotFoundException extends RuntimeException {
  private static final long serialVersionUID = 256315138163873115L;

  public FileNotFoundException(String filePath, Throwable cause) {
    super(String.format("Unable to parse file with path [%s]. File not found.", filePath), cause);
  }
}
