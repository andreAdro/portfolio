package main;

import java.io.File;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import main.callback.Callback;
import main.file.WordFileRunnableParserImpl;
import main.model.wordtracker.WordTracker;
import main.pool.WordRepository;

/** @author Adro */
class WordParser implements Callback {

  private final WordRepository wordRepository;
  private final AtomicInteger numberOfFiles;
  private List<File> files;

  WordParser(WordRepository wordRepository, List<File> files) {
    this.wordRepository = wordRepository;
    this.files = files;
    this.numberOfFiles = new AtomicInteger(files.size());
  }

  void parseFiles() {
    for (File file : files) {
      new Thread(new WordFileRunnableParserImpl(this.wordRepository, file, this)).start();
    }
  }

  @Override
  public void postExecute() {
    if (this.numberOfFiles.decrementAndGet() == 0) {
      writeToConsole();
    }
  }

  private void writeToConsole() {
    List<WordTracker> words = this.wordRepository.getSortedDescData();
    for (WordTracker word : words) {
      System.out.println(word);
    }
  }
}
