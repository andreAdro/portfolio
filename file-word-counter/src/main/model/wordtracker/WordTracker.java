package main.model.wordtracker;

/** Tracks the number of times a word is shown by source @author Adro */
public interface WordTracker {

  /**
   * the word being tracked
   *
   * @return the word
   */
  String getWord();

  /**
   * number of occurrences of provided word
   *
   * @return value matching the number of occurrences
   */
  Integer getOccurrences();

  /**
   * increments the number of occurrences
   *
   * @param source is where the word was found
   */
  void increment(String source);
}
