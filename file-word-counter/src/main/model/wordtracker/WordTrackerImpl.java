package main.model.wordtracker;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/** @author Adro */
public class WordTrackerImpl implements Serializable, WordTracker {
  private static final long serialVersionUID = 7571074158910945386L;

  private final String word;
  private final AtomicInteger counter;
  private Map<String, AtomicInteger> data;

  public WordTrackerImpl(String source, String word) {
    this.word = word;
    this.counter = new AtomicInteger(1);
    this.data = Collections.synchronizedMap(new LinkedHashMap<>());
    this.data.put(source, new AtomicInteger(1));
  }

  @Override
  public String getWord() {
    return this.word;
  }

  public Integer getOccurrences() {
    return counter.get();
  }

  @Override
  public void increment(String source) {
    this.counter.incrementAndGet();
    AtomicInteger atomicInteger = this.data.get(source);
    if (null != atomicInteger) {
      atomicInteger.incrementAndGet();
    } else {
      this.data.put(source, new AtomicInteger(1));
    }
  }

  @Override
  public String toString() {
    String occurrencesInFiles = counterPerFileToString(this.data);
    return String.format("%s %s = %s", this.word, this.counter.get(), occurrencesInFiles);
  }

  private String counterPerFileToString(Map<String, AtomicInteger> data) {
    StringBuilder stringBuilder = new StringBuilder();
    for (AtomicInteger atomicIntegers : data.values()) {
      stringBuilder.append(atomicIntegers.get());
      stringBuilder.append(" + ");
    }
    return stringBuilder
        .toString()
        .substring(0, stringBuilder.length() > 2 ? stringBuilder.length() - 3 : 0);
  }
}
