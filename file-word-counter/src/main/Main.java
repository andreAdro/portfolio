package main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import main.exception.FileNotFoundException;
import main.pool.WordRepositoryImpl;

/** @author Adro */
public class Main {
  public static void main(String[] args) {
    List<File> files = obtainFiles(args);
    new WordParser(WordRepositoryImpl.getInstance(), files).parseFiles();
  }

  private static List<File> obtainFiles(String[] args) {
    List<File> files = new ArrayList<>(args.length);
    for (String arg : args) {
      try {
        files.add(new File(arg));
      } catch (RuntimeException exception) {
        throw new FileNotFoundException(arg, exception);
      }
    }
    return files;
  }
}
