package main.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import main.callback.Callback;
import main.exception.WordParseException;
import main.pool.WordRepository;

/** @author Adro */
public class WordFileRunnableParserImpl implements FileRunnableParser {

  private final WordRepository wordRepository;
  private final File file;
  private final Callback callback;

  public WordFileRunnableParserImpl(WordRepository wordRepository, File file, Callback callback) {
    this.wordRepository = wordRepository;
    this.file = file;
    this.callback = callback;
  }

  @Override
  public void parse() {
    String fileName = this.file.getName();
    try (BufferedReader reader = fileToReader(this.file)) {
      String line;
      while (null != (line = reader.readLine())) {
        populateRepositoryWithLineWords(fileName, line);
      }
    } catch (IOException e) {
      throw new WordParseException(fileName, e);
    }
    this.callback.postExecute();
  }

  private void populateRepositoryWithLineWords(String fileName, String line) {
    String[] words = line.split("\\s+");
    for (String word : words) {
      this.wordRepository.addWord(fileName, word);
    }
  }

  private BufferedReader fileToReader(File file) throws IOException {
    return new BufferedReader(new FileReader(file, StandardCharsets.UTF_8));
  }

  @Override
  public void run() {
    this.parse();
  }
}
