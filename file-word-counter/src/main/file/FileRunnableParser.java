package main.file;

import main.exception.WordParseException;

/** @author Adro */
public interface FileRunnableParser extends Runnable {

  /**
   * parses the file to find words
   *
   * @throws WordParseException if a word does not follow the specification of UTF-8 encoding
   */
  void parse() throws WordParseException;
}
