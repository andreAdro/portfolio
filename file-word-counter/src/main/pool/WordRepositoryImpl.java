package main.pool;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import main.model.wordtracker.WordTracker;
import main.model.wordtracker.WordTrackerImpl;

/** @author Adro */
public class WordRepositoryImpl implements WordRepository {

  private static WordRepository instance = null;
  private static Map<String, WordTracker> data;

  private WordRepositoryImpl() {
    data = Collections.synchronizedMap(new HashMap<>());
  }

  public static WordRepository getInstance() {
    if (null == instance) {
      synchronized (WordRepositoryImpl.class) {
        if (null == instance) {
          instance = new WordRepositoryImpl();
        }
      }
    }
    return instance;
  }

  @Override
  public void addWord(String source, String word) {
    WordTracker wordTracker = data.get(word);
    if (null != wordTracker) {
      wordTracker.increment(source);
    } else {
      data.put(word, new WordTrackerImpl(source, word));
    }
  }

  @Override
  public List<WordTracker> getSortedDescData() {
    return WordRepositoryImpl.data.values().stream()
        .sorted(Comparator.comparingInt(WordTracker::getOccurrences)
        .reversed())
        .collect(Collectors.toList());
  }
}
