package main.pool;

import java.util.List;
import main.model.wordtracker.WordTracker;

/** Repository to contain all know words. @author Adro */
public interface WordRepository {

  /**
   * Adds an word to the repository
   *
   * @param source the source from the word
   * @param word the pretended word to be managed by the WordRepository
   */
  void addWord(String source, String word);

  /**
   * Obtains all the known tracked words by the repository
   *
   * @return a list containing the words sorted by its number of occurrences
   */
  List<WordTracker> getSortedDescData();
}
