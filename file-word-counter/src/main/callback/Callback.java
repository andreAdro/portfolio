package main.callback;

/** @author Adro */
public interface Callback {

  /** Method to be called for a callback behaviour */
  void postExecute();
}
