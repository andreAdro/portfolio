package pt.adro.mobilitytracker.data.bootstrap;

import java.util.Optional;
import pt.adro.mobilitytracker.data.entity.TraceEntity;

/**
 * @author Adro @ 2/17/2020
 */
public interface MobilityDataLineParser {

  /**
   * Parses a line as provided by the examples of given exercice.
   * Should there be a malformed data
   * @param line
   * @return
   */
  Optional<TraceEntity> parseLine(String line);
}
