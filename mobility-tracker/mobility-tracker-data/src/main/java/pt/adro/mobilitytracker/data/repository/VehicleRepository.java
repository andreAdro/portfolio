package pt.adro.mobilitytracker.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.adro.mobilitytracker.data.entity.VehicleEntity;

/** @author Adro @ 2/16/2020 */
@Repository
public interface VehicleRepository extends JpaRepository<VehicleEntity, Long > {}
