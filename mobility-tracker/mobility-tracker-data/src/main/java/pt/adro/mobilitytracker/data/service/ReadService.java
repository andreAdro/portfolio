package pt.adro.mobilitytracker.data.service;

import java.util.Set;

/**
 * @author Adro @ 2/16/2020
 */
public interface ReadService<T, ID> {

  Set<T> findAll();

  T findById(ID id);

}
