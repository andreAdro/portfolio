package pt.adro.mobilitytracker.data.bootstrap;

import java.util.List;
import java.util.Set;
import pt.adro.mobilitytracker.data.entity.FleetEntity;
import pt.adro.mobilitytracker.data.entity.TraceEntity;

/** @author Adro @ 2/17/2020 */
public interface DataMerger {

  Set<FleetEntity> mergeData(List<TraceEntity> traceEntities);

}
