package pt.adro.mobilitytracker.data.service.vehicle;

import pt.adro.mobilitytracker.data.entity.VehicleEntity;
import pt.adro.mobilitytracker.data.service.ReadService;

/** @author Adro @ 2/16/2020 */
public interface VehicleService extends ReadService<VehicleEntity, Long> {}
