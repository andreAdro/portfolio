package pt.adro.mobilitytracker.data.service;

import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import pt.adro.mobilitytracker.data.entity.AbstractEntity;
import pt.adro.mobilitytracker.data.exception.EntityNotFoundException;

/** @author Adro @ 2/16/2020 */
public abstract class AbstractService<T extends AbstractEntity, ID extends Long>
    implements ReadService<T, ID> {

  private JpaRepository<T, ID> repository;

  public AbstractService(JpaRepository<T, ID> repository) {
    this.repository = repository;
  }

  @Override
  public Set<T> findAll() {
    return new LinkedHashSet<>(this.repository.findAll());
  }

  @Override
  public T findById(ID id) {
    return this.repository.findById(id).orElseThrow(EntityNotFoundException::new);
  }
}
