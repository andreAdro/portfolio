package pt.adro.mobilitytracker.data.bootstrap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import pt.adro.mobilitytracker.data.entity.FleetEntity;
import pt.adro.mobilitytracker.data.entity.TraceEntity;
import pt.adro.mobilitytracker.data.service.fleet.FleetService;

/** @author Adro @ 2/17/2020 */
@Component
public class DataLoader implements CommandLineRunner {

  private static final String CLASSPATH_DATA = "classpath:data/siri.20121106.csv";
  private final FleetService fleetService;
  private final ResourceLoader resourceLoader;
  private final MobilityDataLineParser mobilityDataLineParser;
  private final DataMerger dataMerger;

  @Autowired
  public DataLoader(
      FleetService fleetService,
      ResourceLoader resourceLoader,
      MobilityDataLineParser mobilityDataLineParser,
      DataMerger dataMerger) {
    this.fleetService = fleetService;
    this.resourceLoader = resourceLoader;
    this.mobilityDataLineParser = mobilityDataLineParser;
    this.dataMerger = dataMerger;
  }

  @Override
  public void run(String... args) throws Exception {
    if (0 == this.fleetService.findAll().size()) {
      Set<FleetEntity> fleets = extractData();
      this.fleetService.saveFleets(fleets);
    }
  }

  private Set<FleetEntity> extractData() throws IOException {
    InputStream inputStream = this.resourceLoader.getResource(CLASSPATH_DATA).getInputStream();
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
      List<TraceEntity> traceEntities =
          reader.lines()
              .map(this.mobilityDataLineParser::parseLine)
              .map(Optional::get)
              .collect(Collectors.toList());
      return this.dataMerger.mergeData(traceEntities);
    }
  }
}
