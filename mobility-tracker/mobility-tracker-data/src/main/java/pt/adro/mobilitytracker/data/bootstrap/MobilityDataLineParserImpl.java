package pt.adro.mobilitytracker.data.bootstrap;

import java.util.Optional;
import org.springframework.stereotype.Service;
import pt.adro.mobilitytracker.data.entity.FleetEntity;
import pt.adro.mobilitytracker.data.entity.TraceEntity;
import pt.adro.mobilitytracker.data.entity.VehicleEntity;

/** @author Adro @ 2/17/2020 */
@Service
public class MobilityDataLineParserImpl implements MobilityDataLineParser {

  @Override
  public Optional<TraceEntity> parseLine(String line) {
    try {
      FleetEntity fleet = fleetFromLine(line);
      VehicleEntity vehicle = vehicleFromLine(line);
      vehicle.setFleet(fleet);
      TraceEntity trace = traceFromLine(line);
      trace.setVehicle(vehicle);
      return Optional.of(trace);
    } catch (Exception exception) {
      return Optional.empty();
    }
  }

  private FleetEntity fleetFromLine(String line) {
    return new FleetEntity("");
  }

  private VehicleEntity vehicleFromLine(String line) {
    return new VehicleEntity();
  }

  private TraceEntity traceFromLine(String line) {
    return new TraceEntity();
  }
}
