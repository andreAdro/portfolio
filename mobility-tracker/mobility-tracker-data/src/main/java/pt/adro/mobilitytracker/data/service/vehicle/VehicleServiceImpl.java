package pt.adro.mobilitytracker.data.service.vehicle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import pt.adro.mobilitytracker.data.entity.VehicleEntity;
import pt.adro.mobilitytracker.data.repository.VehicleRepository;
import pt.adro.mobilitytracker.data.service.AbstractService;

/** @author Adro @ 2/16/2020 */
@Service
public class VehicleServiceImpl extends AbstractService<VehicleEntity, Long>
    implements VehicleService {

  @Autowired
  public VehicleServiceImpl(VehicleRepository repository) {
    super(repository);
  }
}
