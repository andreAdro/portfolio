package pt.adro.mobilitytracker.data.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pt.adro.mobilitytracker.domain.activity.Trace;
import pt.adro.mobilitytracker.domain.fleet.Fleet;
import pt.adro.mobilitytracker.domain.vehicle.Vehicle;

/** @author Adro @ 2/16/2020 */
@Entity(name = "VEHICLES")
@Data
@NoArgsConstructor
@ToString(callSuper = true, exclude = "fleet")
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class VehicleEntity extends AbstractEntity implements Vehicle, Serializable {

  private static final long serialVersionUID = -6702034442090282189L;

  @ManyToOne(fetch = FetchType.EAGER, targetEntity = FleetEntity.class)
  @JoinColumn(name = "FLEET_ID")
  private Fleet fleet;

  @OneToMany(fetch = FetchType.LAZY, targetEntity = TraceEntity.class, mappedBy = "vehicle")
  private Set<Trace> traces = new HashSet<>();

  public VehicleEntity(String id) {
    super(id);
  }
}
