package pt.adro.mobilitytracker.data.bootstrap;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import pt.adro.mobilitytracker.data.entity.FleetEntity;
import pt.adro.mobilitytracker.data.entity.TraceEntity;
import pt.adro.mobilitytracker.data.entity.VehicleEntity;

/** @author Adro @ 2/17/2020 */
@Service
public class DataMergerImpl implements DataMerger {

  private final Map<String, FleetEntity> fleets = new HashMap<>();
  private final Map<String, VehicleEntity> vehicles = new HashMap<>();

  @Override
  public Set<FleetEntity> mergeData(List<TraceEntity> traceEntities) {

    return null;
  }
}
