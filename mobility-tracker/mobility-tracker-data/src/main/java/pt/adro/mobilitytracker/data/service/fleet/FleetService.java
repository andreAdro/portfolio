package pt.adro.mobilitytracker.data.service.fleet;

import java.util.Set;
import pt.adro.mobilitytracker.data.entity.FleetEntity;
import pt.adro.mobilitytracker.data.service.ReadService;
import pt.adro.mobilitytracker.domain.fleet.Fleet;

/**
 * @author Adro @ 2/16/2020
 *     <p>Service responsible for hanfling Fleet data management. Should be proxied if the
 *     application growns in size
 */
public interface FleetService extends ReadService<FleetEntity, Long> {

  void saveFleets(Set<FleetEntity> fleets);
}
