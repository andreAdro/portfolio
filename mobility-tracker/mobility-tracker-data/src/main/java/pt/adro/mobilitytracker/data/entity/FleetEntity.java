package pt.adro.mobilitytracker.data.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pt.adro.mobilitytracker.domain.fleet.Fleet;
import pt.adro.mobilitytracker.domain.vehicle.Vehicle;

/** @author Adro @ 2/16/2020 */
@Entity(name = "FLEETS")
@Data
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class FleetEntity extends AbstractEntity implements Fleet, Serializable {

  private static final long serialVersionUID = -7417534587714338460L;

  @OneToMany(fetch = FetchType.LAZY, targetEntity = VehicleEntity.class, mappedBy = "fleet")
  private Set<Vehicle> vehicles = new HashSet<>();

  public FleetEntity(String id) {
    super(id);
  }
}
