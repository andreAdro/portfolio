package pt.adro.mobilitytracker.data.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pt.adro.mobilitytracker.domain.activity.Trace;
import pt.adro.mobilitytracker.domain.fleet.Fleet;
import pt.adro.mobilitytracker.domain.vehicle.Vehicle;

/** @author Adro @ 2/16/2020 */
@Entity(name = "TRACES")
@Data
@NoArgsConstructor
@ToString(exclude = {"vehicle"})
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TraceEntity implements Trace, Serializable {

  private static final long serialVersionUID = -4299133582444769415L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;

  @EqualsAndHashCode.Include
  @Column(name = "TIME_STAMP")
  private Timestamp timestamp;

  @EqualsAndHashCode.Include
  @ManyToOne(fetch = FetchType.LAZY, targetEntity = VehicleEntity.class)
  @JoinColumn(name = "VEHICLE_ID")
  private Vehicle vehicle;

  @Column(name = "START_TIME")
  private Timestamp startTime;

  @Column(name = "LONGITUDE")
  private Double longitude;

  @Column(name = "LATITUDE")
  private Double latitude;

  @Column(name = "IS_AT_STOP")
  private Boolean isAtStop;

  @Override
  public Boolean isAtStop() {
    return isAtStop;
  }

  @Override
  public Fleet getFleet() {
    return vehicle.getFleet();
  }
}
