package pt.adro.mobilitytracker.data.service.fleet;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.adro.mobilitytracker.data.entity.FleetEntity;
import pt.adro.mobilitytracker.data.repository.FleetRepository;
import pt.adro.mobilitytracker.data.service.AbstractService;

/** @author Adro @ 2/16/2020 */
@Service
public class FleetServiceImpl extends AbstractService<FleetEntity, Long> implements FleetService {

  private final FleetRepository repository;

  @Autowired
  public FleetServiceImpl(FleetRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public void saveFleets(Set<FleetEntity> fleets) {
    this.repository.saveAll(fleets);
  }
}
