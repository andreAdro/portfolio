package pt.adro.mobilitytracker.domain.vehicle;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serializable;
import java.util.Set;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pt.adro.mobilitytracker.domain.AbstractDomain;
import pt.adro.mobilitytracker.domain.activity.Trace;
import pt.adro.mobilitytracker.domain.activity.TraceDTO;
import pt.adro.mobilitytracker.domain.fleet.FleetDTO;

/**
 * @author Adro @ 2/16/2020
 *     <p>Simple POJO class to instantiate a Vehicle interface
 */
@Data
@ToString(callSuper = true, exclude = "fleet")
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class VehicleDTO extends AbstractDomain implements Vehicle, Serializable {

  private static final long serialVersionUID = 7166375270124439560L;

  private FleetDTO fleet;

  @JsonDeserialize(contentAs = TraceDTO.class)
  private Set<Trace> traces;
}
