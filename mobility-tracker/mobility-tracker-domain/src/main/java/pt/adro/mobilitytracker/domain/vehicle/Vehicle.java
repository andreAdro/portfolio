package pt.adro.mobilitytracker.domain.vehicle;

import java.util.Set;
import pt.adro.mobilitytracker.domain.Domain;
import pt.adro.mobilitytracker.domain.activity.Trace;
import pt.adro.mobilitytracker.domain.fleet.Fleet;

/**
 * @author Adro @ 2020-02-13
 */
public interface Vehicle extends Domain {

  Fleet getFleet();

  Set<Trace> getTraces();
}
