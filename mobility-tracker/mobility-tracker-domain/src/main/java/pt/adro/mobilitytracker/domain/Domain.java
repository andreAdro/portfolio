package pt.adro.mobilitytracker.domain;

/** @author Adro @ 2/16/2020 */
public interface Domain {

  String getId();
}
