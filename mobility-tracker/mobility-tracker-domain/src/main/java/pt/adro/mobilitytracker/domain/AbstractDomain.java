package pt.adro.mobilitytracker.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/** @author Adro @ 2/16/2020 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
public abstract class AbstractDomain implements Domain {

  @EqualsAndHashCode.Include private String id;
}
