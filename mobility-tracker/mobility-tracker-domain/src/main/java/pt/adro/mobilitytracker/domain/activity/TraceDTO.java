package pt.adro.mobilitytracker.domain.activity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serializable;
import java.sql.Timestamp;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pt.adro.mobilitytracker.domain.fleet.Fleet;
import pt.adro.mobilitytracker.domain.vehicle.Vehicle;
import pt.adro.mobilitytracker.domain.vehicle.VehicleDTO;

/**
 * @author Adro @ 2/16/2020
 *     <p>Simple POJO class to instantiate a Trace interface
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(exclude = {"vehicle"})
public class TraceDTO implements Trace, Serializable {

  private static final long serialVersionUID = -8250992171539589163L;

  @EqualsAndHashCode.Include private Timestamp timestamp;

  @EqualsAndHashCode.Include
  @JsonDeserialize(contentAs = VehicleDTO.class)
  private Vehicle vehicle;

  private Timestamp startTime;
  private Double longitude;
  private Double latitude;
  private Boolean isAtStop;

  @Override
  public Boolean isAtStop() {
    return isAtStop;
  }

  @Override
  public Fleet getFleet() {
    return this.vehicle.getFleet();
  }
}
