package pt.adro.mobilitytracker.domain.fleet;

import java.util.Set;
import pt.adro.mobilitytracker.domain.Domain;
import pt.adro.mobilitytracker.domain.vehicle.Vehicle;

/**
 * @author Adro @ 2020-02-13
 */
public interface Fleet extends Domain {

  Set<Vehicle> getVehicles();

}
