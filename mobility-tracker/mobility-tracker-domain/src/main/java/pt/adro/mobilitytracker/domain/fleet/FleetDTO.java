package pt.adro.mobilitytracker.domain.fleet;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serializable;
import java.util.Set;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pt.adro.mobilitytracker.domain.AbstractDomain;
import pt.adro.mobilitytracker.domain.vehicle.Vehicle;
import pt.adro.mobilitytracker.domain.vehicle.VehicleDTO;

/**
 * @author Adro @ 2/16/2020
 *     <p>Simple POJO class to instantiate a Fleet interface
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class FleetDTO extends AbstractDomain implements Fleet, Serializable {
  private static final long serialVersionUID = -2290982740642358921L;

  @JsonDeserialize(contentAs = VehicleDTO.class)
  private Set<Vehicle> vehicles;
}
