package pt.adro.mobilitytracker.domain.activity;

import java.sql.Timestamp;
import pt.adro.mobilitytracker.domain.fleet.Fleet;
import pt.adro.mobilitytracker.domain.vehicle.Vehicle;

/** @author Adro @ 2020-02-13 */
public interface Trace {

  Timestamp getTimestamp();

  //  Long lineId(); // Ignored for sake of brevity

  //  Long getDirection(); // Ignored for sake of brevity

  //  String getJourneyPatternId(); // Ignored for sake of brevity

  Timestamp getStartTime();

  //  Long getVehicleJourneyId(); // Ignored for sake of brevity

  Fleet getFleet(); // Operator

  //  Boolean isConditioned(); // Ignored for sake of brevity

  Double getLongitude();

  Double getLatitude();

  //  Long getDelayInSeconds(); // Ignored for sake of brevity

  //  Long getBlockId(); // Ignored for sake of brevity

  Vehicle getVehicle();

  //  Long getStopId(); // Ignored for sake of brevity

  Boolean isAtStop();
}
