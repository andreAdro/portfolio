package pt.adro.mobilitytracker.web.controller;

import lombok.extern.slf4j.Slf4j;
import pt.adro.mobilitytracker.web.exception.BadRequest;

/** @author Adro @ 2/16/2020 */
public abstract class AbstractController {

  public AbstractController() {}

  protected Long parseId(String id) {
    try {
      return Long.valueOf(id);
    } catch (NumberFormatException exception) {
      throw new BadRequest("Provided id [{}] is invalid", exception);
    }
  }
}
