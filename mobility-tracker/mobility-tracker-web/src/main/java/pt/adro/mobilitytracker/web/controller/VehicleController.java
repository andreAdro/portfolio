package pt.adro.mobilitytracker.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.adro.mobilitytracker.data.service.vehicle.VehicleService;
import pt.adro.mobilitytracker.domain.vehicle.Vehicle;
import pt.adro.mobilitytracker.web.mapper.VehicleMapper;

/** @author Adro @ 2/16/2020 */
@RestController
@RequestMapping("/vehicles")
public class VehicleController extends AbstractController {

  private final VehicleService vehicleService;
  private final VehicleMapper vehicleMapper;

  @Autowired
  public VehicleController(VehicleService vehicleService, VehicleMapper vehicleMapper) {
    super();
    this.vehicleService = vehicleService;
    this.vehicleMapper = vehicleMapper;
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<Vehicle> getFleetById(@PathVariable("id") String id) {
    Long longId = parseId(id);
    return ResponseEntity.ok()
        .body(vehicleMapper.vehicleToDTO(this.vehicleService.findById(longId)));
  }
}
