package pt.adro.mobilitytracker.web.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;
import pt.adro.mobilitytracker.domain.fleet.Fleet;
import pt.adro.mobilitytracker.domain.fleet.FleetDTO;

/**
 * @author Adro @ 2/16/2020
 */
@Mapper(componentModel = "spring")
@Component
public interface FleetMapper {

  FleetMapper INSTANCE = Mappers.getMapper(FleetMapper.class);

  FleetDTO fleetToDTO(Fleet fleet);
}
