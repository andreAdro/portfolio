package pt.adro.mobilitytracker.web.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pt.adro.mobilitytracker.web.exception.BadRequest;

/** @author Adro @ 2/16/2020 */
public class BadRequestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(value = BadRequest.class)
  ResponseEntity<Object> handleConflict(RuntimeException exception, WebRequest request) {
    return handleExceptionInternal(
        exception, exception.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
  }
}
