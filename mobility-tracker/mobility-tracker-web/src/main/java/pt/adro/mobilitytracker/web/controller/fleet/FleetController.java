package pt.adro.mobilitytracker.web.controller.fleet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.adro.mobilitytracker.data.service.fleet.FleetService;
import pt.adro.mobilitytracker.domain.fleet.Fleet;
import pt.adro.mobilitytracker.web.controller.AbstractController;
import pt.adro.mobilitytracker.web.mapper.FleetMapper;

/** @author Adro @ 2/16/2020 */
@RequestMapping("/fleets")
@RestController
public class FleetController extends AbstractController {

  private final FleetService fleetService;
  private final FleetMapper fleetMapper;

  @Autowired
  public FleetController(FleetService fleetService, FleetMapper fleetMapper) {
    super();
    this.fleetService = fleetService;
    this.fleetMapper = fleetMapper;
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<Fleet> getFleetById(@PathVariable("id") String id) {
    Long longId = parseId(id);
    return ResponseEntity.ok().body(fleetMapper.fleetToDTO(this.fleetService.findById(longId)));
  }
}
