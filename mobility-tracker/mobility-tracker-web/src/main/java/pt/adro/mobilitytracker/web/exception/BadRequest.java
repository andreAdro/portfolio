package pt.adro.mobilitytracker.web.exception;

/** @author Adro @ 2/16/2020 */
public class BadRequest extends RuntimeException {

  public BadRequest() {}

  public BadRequest(String message) {
    super(message);
  }

  public BadRequest(String message, Throwable cause) {
    super(message, cause);
  }

  public BadRequest(Throwable cause) {
    super(cause);
  }

  public BadRequest(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
