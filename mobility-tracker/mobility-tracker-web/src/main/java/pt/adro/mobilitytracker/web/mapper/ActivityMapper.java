package pt.adro.mobilitytracker.web.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;
import pt.adro.mobilitytracker.domain.activity.Trace;
import pt.adro.mobilitytracker.domain.activity.TraceDTO;

/** @author Adro @ 2/16/2020 */
@Mapper(componentModel = "spring")
@Component
public interface ActivityMapper {

  ActivityMapper INSTANCE = Mappers.getMapper(ActivityMapper.class);

  TraceDTO activityToDEO(Trace trace);
}
