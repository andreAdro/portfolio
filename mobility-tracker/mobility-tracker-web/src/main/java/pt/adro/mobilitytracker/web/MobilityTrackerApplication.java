package pt.adro.mobilitytracker.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "pt.adro")
@EnableJpaRepositories(basePackages  = {"pt.adro"})
@EntityScan(basePackages  = {"pt.adro"})
public class MobilityTrackerApplication {

  public static void main(String[] args) {
    SpringApplication.run(MobilityTrackerApplication.class, args);
  }
}
