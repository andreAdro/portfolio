package pt.adro.mobilitytracker.web.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;
import pt.adro.mobilitytracker.domain.vehicle.Vehicle;
import pt.adro.mobilitytracker.domain.vehicle.VehicleDTO;

/** @author Adro @ 2/16/2020 */
@Mapper(componentModel = "spring")
@Component
public interface VehicleMapper {

  VehicleMapper INSTANCE = Mappers.getMapper(VehicleMapper.class);

  VehicleDTO vehicleToDTO(Vehicle vehicle);
}
